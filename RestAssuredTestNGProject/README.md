# Rest Assured TestNG Project

# Introduction:
Rest Assured is a popular Java library used for testing and validating REST APIs. It simplifies the process of making HTTP requests and verifying responses. Rest Assured have POST, GET, PUT, PATCH & DELETE requests, and TestNG is a testing framework. When we combine Rest Assured with TestNG, it become a powerful testing framework for Java, and a effective tool for automating API testing.
We have tested various functionalities in this project.

- Data Driven Testing & Data Provider
Data-driven testing with TestNG allows us to run the same test multiple times with different sets of data. Various conditions are verified using data driven testing in our project. You can achieve data-driven testing in TestNG using @DataProvider annotation or reading data from Excel or CSV file aslo. With @DataPrivider annotations we can provide a set of data and run the test multiple times. Also by using @Parametete annotaion we have passed parameters to the test method.

- Series Runner
With TestNG, a series runner means tests can be run in a sequence. Series runner is useful when result of one test is depends on other. Until and unless first test runs successfully and returns the response, the next test will not execute. To achieve this, we have used TestNG features such as @Test annotation.

- Parallel Runner with Test,Method & class
Running tests in parallel is used speed up the execution time for large test suites. TestNG support parallel test execution to run the test suits, class, methods or test in parallel. We configured parallel execution at various levels such as classes, methods, tests, and suites.

-Priority
priority is used to define the order in which test methods are executed within a test class. In our framework we have given prioroty to the test method and tested the priority functionality with testng.







