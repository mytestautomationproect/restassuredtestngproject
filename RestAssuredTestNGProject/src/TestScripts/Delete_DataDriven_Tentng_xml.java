package TestScripts;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utilities;
import Common_Utilities.testng_retry_analyzer;
import io.restassured.response.Response;

public class Delete_DataDriven_Tentng_xml extends API_Trigger {

	File logfolder;
	Response response;
	
	@BeforeTest
	public void setup() {
		logfolder = Utilities.create_folder("Delete_API");
	}

	@Parameters({"req_name","req_job"})
	@Test (retryAnalyzer = testng_retry_analyzer.class , description = "Validate the responseBody Parameter of Delete_TC_01")
	public void validate_Delete_API(String req_name, String req_job) {
		
		String requestBody = "";
		
	
	response = Delete_API_Trigger(requestBody , delete_endpoint());
	
	// Step 5: Extract the status code
	int statuscode = response.statusCode();
	System.out.println(statuscode);
	
	//Assert.assertNull(null, "Data has been deleted");
	Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times"); //201 for retry/204 for pass
	}
	
	@AfterTest
	public void teardown() throws IOException {
		Utilities.create_log_file("Delete_API_TC01", logfolder, delete_endpoint(), delete_request_body(),null,null);
	}


}

