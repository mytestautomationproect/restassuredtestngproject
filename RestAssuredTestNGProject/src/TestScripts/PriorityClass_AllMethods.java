package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Common_Methods.API_Trigger;
import Common_Methods.Utilities;
import Common_Utilities.testng_retry_analyzer;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class PriorityClass_AllMethods extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup_post() {
		logfolder = Utilities.create_folder("Post_API");
	}

	@BeforeTest
	public void setup_put() {
		logfolder = Utilities.create_folder("Put_API");
	}
	
	@Test (priority = 1, retryAnalyzer = testng_retry_analyzer.class , description = "Validate the responseBody Parameter of Post_TC_01")
	public void validate_Post_API() {
		response = Post_API_Trigger(post_request_body(), post_endpoint());
		int statuscode = response.statusCode();
		System.out.println(statuscode);
		responseBody = response.getBody();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(post_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");  //200 for retry/201 for pass
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}
	

	@Test (priority = 2, retryAnalyzer = testng_retry_analyzer.class , description = "Validate the responseBody Parameter of Put_TC_01")
	public void validate_Put_API() {
		response = Put_API_Trigger(put_request_body(), put_endpoint());

		int statuscode = response.statusCode();
		System.out.println(statuscode);

		responseBody = response.getBody();
		System.out.println(responseBody.asString());

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);

		JsonPath jsp_req = new JsonPath(put_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0, 11);
		
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");  // 201 for retry/200 for pass
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, exp_date, "createdAt in ResponseBody is equal to Date Generated");

	}
	
	@AfterTest
	public void teardown_post() throws IOException {
		Utilities.create_log_file("Post_API_TC01", logfolder, post_endpoint(), post_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}

	@AfterTest
	public void teardown_put() throws IOException {
		Utilities.create_log_file("Put_API_TC01", logfolder, put_endpoint(), put_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}

}
