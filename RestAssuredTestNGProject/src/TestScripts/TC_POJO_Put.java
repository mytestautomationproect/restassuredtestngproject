package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import Common_Methods.API_Trigger;
import Common_Methods.Utilities;
import Common_Utilities.testng_retry_analyzer;
import Environment_and_Repository.Req_Repo_POJO;
import POJO.POJO_Post_Api;
import POJO.POJO_PutPatch_API;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

	public class TC_POJO_Put extends API_Trigger {

		File logfolder;
		Response response;
		Req_Repo_POJO req_obj = new Req_Repo_POJO();

		@BeforeTest
		public void setup() {
			logfolder = Utilities.create_folder("Put_API");
		}

		@Test (retryAnalyzer = testng_retry_analyzer.class)
		public void validate_Put_API() throws JsonProcessingException {
			response = Put_API_Trigger(req_obj.Put_TC01(), put_endpoint());

			int statuscode = response.statusCode();
			System.out.println(statuscode);

			POJO_PutPatch_API responseBody = response.as(POJO_PutPatch_API.class);

			String res_name = responseBody.getName();
			String res_job = responseBody.getJob();
			String res_updatedAt = responseBody.getUpdatedAt();
			res_updatedAt = res_updatedAt.substring(0, 11);

			JsonPath jsp_req = new JsonPath(req_obj.Put_TC01());
			String req_name = jsp_req.getString("name");
			String req_job = jsp_req.getString("job");

			LocalDateTime curr_date = LocalDateTime.now();
			String exp_date = curr_date.toString().substring(0, 11);
			
			Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");  // 201 for retry/200 for pass
			Assert.assertEquals(res_name, req_name, "Name in ResponseBody is equal to Name sent in Request Body");
			Assert.assertEquals(res_job, req_job, "Job in ResponseBody is equal to Job sent in Request Body");
			Assert.assertEquals(res_updatedAt, exp_date, "createdAt in ResponseBody is equal to Date Generated");

		}

		@AfterTest
		public void teardown() throws IOException {
			Utilities.create_log_file("Put_API_TC01", logfolder, put_endpoint(), put_request_body(),
					response.getHeaders().toString(), response.getBody().asString());
		}

	}


