package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Common_Methods.API_Trigger;
import Common_Methods.Utilities;
import Common_Utilities.testng_retry_analyzer;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_TestScript extends API_Trigger {

	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.create_folder("Patch_API");
	}

	@Test (retryAnalyzer = testng_retry_analyzer.class , description = "Validate the responseBody Parameter of Patch_TC_01")
	public void validate_Patch_API() {

		response = Patch_API_Trigger(patch_request_body(), patch_endpoint());

		int statuscode = response.statusCode();
		System.out.println(statuscode);

		responseBody = response.getBody();
		System.out.println(responseBody.asString());

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(patch_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime curr_date = LocalDateTime.now();
		String exp_date = curr_date.toString().substring(0, 11);
		
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");  //201 for retry/200 for pass
		Assert.assertEquals(res_name, req_name, "Name in responseBody is equal to name sent from requestBody");
		Assert.assertEquals(res_job, req_job, "Job in responseBody is equal to job sent from requestBody");
		Assert.assertEquals(res_updatedAt, exp_date, "updatedAt in responseBody is equal to updatedAt sent from requestBody");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.create_log_file("Patch_API_TC01", logfolder, patch_endpoint(), patch_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}

}
