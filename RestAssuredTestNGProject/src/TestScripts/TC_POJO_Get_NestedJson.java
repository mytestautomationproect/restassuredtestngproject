package TestScripts;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import Common_Methods.API_Trigger;
import Common_Methods.Utilities;
import Common_Utilities.testng_retry_analyzer;
import POJO.POJO_GetApi_DataArray;
import POJO.POJO_GetApi_Parent;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class TC_POJO_Get_NestedJson extends API_Trigger {

	File logfolder;
	Response response;
	POJO_GetApi_Parent responseBody;

	@BeforeTest
	public void setup() {
		logfolder = Utilities.create_folder("Get_API");
	}

	@Test (retryAnalyzer = testng_retry_analyzer.class , description = "Validate the responseBody Parameter of Get_TC_01")
	public void validate_Get_API() {
		
		response = Get_API_Trigger(get_endpoint());
		responseBody = response.as(POJO_GetApi_Parent.class);
		
		int statuscode = response.statusCode();
		System.out.println(statuscode);
		
		
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		int[] exp_id = { 7, 8, 9, 10, 11, 12 };
		String[] exp_email = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String[] exp_first_name = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String[] exp_last_name = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String[] exp_avatar = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };


		int res_page = responseBody.getPage();
		int res_per_page = responseBody.getPer_page();
		int res_total = responseBody.getTotal();
		int res_total_pages = responseBody.getTotal_pages();

		List<POJO_GetApi_DataArray> dataArray = responseBody.getData();
		int sizeofarray = dataArray.size();

		Assert.assertEquals(res_page, exp_page);
		Assert.assertEquals(res_per_page, exp_per_page);
		Assert.assertEquals(res_total, exp_total);
		Assert.assertEquals(res_total_pages, exp_total_pages);
		
		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");  //204 for retry/200 for pass

		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(dataArray.get(i).getId(), exp_id[i], "Validation of id failed : " + i);
			Assert.assertEquals(dataArray.get(i).getEmail(), exp_email[i], "Validation of email failed : " + i);
			Assert.assertEquals(dataArray.get(i).getFirst_name(), exp_first_name[i], "Validation of first_name failed : " + i);
			Assert.assertEquals(dataArray.get(i).getLast_name(), exp_last_name[i], "Validation of last_name failed : " + i);
			Assert.assertEquals(dataArray.get(i).getAvatar(), exp_avatar[i], "Validation of avatar failed : " + i);
		}

		String exp_url = "https://reqres.in/#support-heading";
		String exp_text = "To keep ReqRes free, contributions towards server costs are appreciated!";

		Assert.assertEquals(responseBody.getSupport().getUrl(), exp_url, "Validation of URL failed");
		Assert.assertEquals(responseBody.getSupport().getText(), exp_text, "Validation of text failed");
	}

	private Response Get_API_Trigger(String get_requestbody, String get_endpoint) {
		// TODO Auto-generated method stub
		return null;
	}

	private String get_requestbody() {
		// TODO Auto-generated method stub
		return null;
	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.create_log_file("Get_API_TC01", logfolder, get_endpoint(), null,
				response.getHeaders().toString(), response.getBody().asString());
	}

}
