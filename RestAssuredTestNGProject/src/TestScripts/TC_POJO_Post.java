package TestScripts;

import static org.testng.AssertJUnit.assertEquals;
import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.fasterxml.jackson.core.JsonProcessingException;
import Common_Methods.API_Trigger;
import Common_Methods.Utilities;
import Common_Utilities.testng_retry_analyzer;
import Environment_and_Repository.Req_Repo_POJO;
import POJO.POJO_Post_Api;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;

public class TC_POJO_Post extends API_Trigger {
	File logfolder;
	Response response;
	Req_Repo_POJO req_obj = new Req_Repo_POJO();

	@BeforeTest
	public void setup() {
		logfolder = Utilities.create_folder("Post_API");
	}

	@Test (retryAnalyzer = testng_retry_analyzer.class)
	public void validate_Post_API() throws JsonProcessingException {
		response = Post_API_Trigger(req_obj.Post_TC01(), post_endpoint());
		int statuscode = response.statusCode();
		System.out.println(statuscode);
		
		POJO_Post_Api responseBody = response.as(POJO_Post_Api.class);
		
		String res_name = responseBody.getName();
		String res_job = responseBody.getJob();
		String res_id = responseBody.getId();
		String res_createdAt = responseBody.getCreatedAt();
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(req_obj.Post_TC01());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		
		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 5 times");  //200 for retry/201 for pass
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {
		Utilities.create_log_file("Post_API_TC01", logfolder, post_endpoint(), post_request_body(),
				response.getHeaders().toString(), response.getBody().asString());
	}

}