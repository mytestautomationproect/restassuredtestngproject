package POJO;

import java.util.List;

public class POJO_GetApi_Parent {

	public int getPage() {
		return page;
	}

	public int getPer_page() {
		return per_page;
	}

	public int getTotal() {
		return total;
	}

	public int getTotal_pages() {
		return total_pages;
	}

	public List<POJO_GetApi_DataArray> getData() {
		return data;
	}

	public POJO_GetApi_Support getSupport() {
		return support;
	}

	private int page;
	private int per_page;
	private int total;
	private int total_pages;
	private List<POJO_GetApi_DataArray> data;
	private POJO_GetApi_Support support;

}
