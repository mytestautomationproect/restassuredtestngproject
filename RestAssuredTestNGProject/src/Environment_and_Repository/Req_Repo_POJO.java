package Environment_and_Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import POJO.POJO_Post_Api;
import POJO.POJO_PutPatch_API;

public class Req_Repo_POJO {
	
	ObjectMapper objMap = new ObjectMapper();
	
	POJO_Post_Api postpojoobj = new POJO_Post_Api();
	POJO_PutPatch_API PutPatchObj = new POJO_PutPatch_API();
	
	public String Post_TC01() throws JsonProcessingException {
		postpojoobj.setName("morpheus");
		postpojoobj.setJob("leader");
		
		String requestBody = objMap.writeValueAsString(postpojoobj);
		return requestBody;
	}
	
	public String Put_TC01() throws JsonProcessingException {
		postpojoobj.setName("morpheus");
		postpojoobj.setJob("zion resident");
		
		String requestBody = objMap.writeValueAsString(PutPatchObj);
		return requestBody;
	}
	
	public String Patch_TC01() throws JsonProcessingException {
		postpojoobj.setName("morpheus");
		postpojoobj.setJob("zion resident");
		
		String requestBody = objMap.writeValueAsString(PutPatchObj);
		return requestBody;
	}
	

}
